Content[:]Allow the group to take a break for the day while you stay on guard
Consequence[:]By allowing the group to take a break, you've improved their mood. You've worked an extra shift to protect the farm. 
PlayerMorale[:]-10
GroupMorale[:]+20
Perk[:]null