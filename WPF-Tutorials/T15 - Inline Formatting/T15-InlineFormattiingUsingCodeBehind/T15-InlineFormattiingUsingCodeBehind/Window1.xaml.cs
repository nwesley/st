﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace T15_InlineFormattiingUsingCodeBehind
{
    /// <summary>
    /// Tutorial 15 - TextBlock Control Inline Formatting using Code-Behind
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            TextBlock tb = new TextBlock();
            tb.Inlines.Add("This is an example of ");
            tb.TextWrapping = TextWrapping.Wrap;
            tb.Margin = new Thickness(10);
            tb.Inlines.Add(new Run("the textblock control ") {FontWeight=FontWeights.Bold});
            tb.Inlines.Add("using");
            tb.Inlines.Add(new Run(" inline") { FontStyle = FontStyles.Italic });
            tb.Inlines.Add(new Run(" text formatting ") {  Foreground=Brushes.Blue});
            tb.Inlines.Add("from ");
            tb.Inlines.Add(new Run("Code-Behind ") { TextDecorations = TextDecorations.Underline });
            tb.Inlines.Add(".");
            this.Content = tb;
        }
    }
}
