﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace T12_CodeBehindResources
{
    /// <summary>
    /// Tutorial 12 - Retriving and the Use of Resources From Code-Behind
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnClickMe_Click(object sender, RoutedEventArgs e)
        {
            lbResult.Items.Add(pnlMain.FindResource("strPanel").ToString());//Searches for the resource within the panel resource section of [MainWindow.xaml] file
            lbResult.Items.Add(this.FindResource("strWindow").ToString());//Searches for the resource within the window resource section of [MainWindow.xaml] file
            lbResult.Items.Add(Application.Current.FindResource("strApp").ToString());//Searches for the resource within the app resource section of [app.xaml] file
        }
    }
}
