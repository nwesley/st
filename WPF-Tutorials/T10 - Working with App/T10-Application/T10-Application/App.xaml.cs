﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace T10_Application
{
    /// <summary>
    /// Tutorial 10 - Modify the app.xaml & app.xaml.cs files to change the default start up window when application is executed
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(Object sender, StartupEventArgs e)
        {
            MainWindow wnd = new MainWindow(); //Creates the new window
            //Can change properties of the window here
            wnd.Title = "Changed Start-Up Window from default by modifying App.xaml";
            wnd.Show();
        }
    }
}
