﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;


namespace T11_CommandLineParameters
{
    /// <summary>
    /// Tutorial 11 - Modifying files to pass command-line parameters on start up events (eg. Opening a file upton startup)
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup (object sender, StartupEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            if (e.Args.Length==1)
            {
                MessageBox.Show("Opening File: \n\n" + e.Args[0]);
                //Here you can open the file passed through the command line parameter which will open upon start up
                //wnd.OpenFile(e.Args[0]);
                //It would be better have have an own class & method to perform this OpenFile Dialog
                wnd.Show();
            }
        }
    }
}
