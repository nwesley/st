﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace T13_HandlingExceptions
{
    /// <summary>
    /// Tutorial 13 - Understanding the difference between uhnadled & handled 
    /// exceptions modifying (window.xaml) > (window.xaml.cs) > (app.xaml) > (app.xaml.cs)
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledExceptions(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("An unhandled exception occured:" + e.Exception.Message, "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
            e.Handled = true;//Will set to true, to show WPF that it's done with this exception and nothing else needs to be done with it
        }
    }
}
