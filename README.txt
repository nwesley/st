##INFORMATION ABOUT COMP6207-SPECIALTOPIC##

This repository is a collection of WPF applications developed within Visual Studio 2017. Each tutorial will demonstrate a new skill learned through the duration COMP6207 Special Topic Study Course. 

### What is this repository for? ###

* Collection of applications are to support study material.
* StudyLog text file to document how many hours spent through the duration of course. 

## Assessments ##
*Assessment 1 - Adventure Story-Game (filename: TWD.exe) - Interface that reads from text file resources, deciding multiple paths
*Assessment 3 - Diary Application (filename: DiaryApp.exe) - Interface that retrieves, updates and deletes data from a database


### How do I get set up? ###

* First, you will need to clone this repository to your local machine
* You will require Visual Studio 2017 integrated development editor to be able to run applications
* 1 - Select & open a tutorial folder you would like to examine. 
* 2 - Click onto the solution (.sln) file and Visual Studio will open the project folder with all componments required to run the WPF application
* 3 - Once Visual Studio is open and loaded, PRESS F5 to run the application
* 4 - You can close the pop-up window when you've finished testing the application. 
* 5 - You can close the Visual Studio application window when finished examining C# & XAML code
* Repeated process 1 - 5 when opening different tutorial applications

### For more information ###
* Contact Owner: Nadia Wesley (9996822) : nwesley49@gmail.com